﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Anasayfa.Master" AutoEventWireup="true" CodeBehind="kategori.aspx.cs" Inherits="Diziblog.kategori" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="content-grids">
            <div class="col-md-8 content-main">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <div class="content-grid">
                            <div class="content-grid-info">
                                <img src="<%# Eval("GORSEL") %>" alt="" style="height: 300px; width: 600px" />
                                <div class="post-info">
                                    <h4><a href="ıcerık.aspx?BLOGID=<%# Eval("BLOGID") %>"><%# Eval("BASLIK") %></a> "<%# Eval("TARIH") %>"</h4>
                                    <p>"<%# Eval("ICERIK") %>"</p>
                                    <a href="single.html"><span></span>DAHA FAZLA OKU </a>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="col-md-4 content-right">
            <div class="recent">
                <h3>SON KONULAR</h3>
                <ul>
                    <asp:Repeater ID="Repeater3" runat="server">
                        <ItemTemplate>
                            <li><a href="#"><%# Eval("BASLIK") %></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="comments">
                <h3>SON YORUMLAR</h3>
                <ul>
                    <li><a href="#">Amada Doe </a>on <a href="#">Hello World!</a></li>
                    <li><a href="#">Peter Doe </a>on <a href="#">Photography</a></li>
                    <li><a href="#">Steve Roberts  </a>on <a href="#">HTML5/CSS3</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="archives">
                <h3>ARŞIV</h3>
                <ul>
                    <li><a href="#">October 2013</a></li>
                    <li><a href="#">September 2013</a></li>
                    <li><a href="#">August 2013</a></li>
                    <li><a href="#">July 2013</a></li>
                </ul>
            </div>
            <div class="categories">
                <h3>KATEGORİ</h3>
                <ul>
                    <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>
                            <li><a href="kategori.aspx?KATEGORIID=<%# Eval("KATEGORIID") %>"><%# Eval("KATEGORIAD") %></a></li>

                        </ItemTemplate>
                    </asp:Repeater>

                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

</asp:Content>

