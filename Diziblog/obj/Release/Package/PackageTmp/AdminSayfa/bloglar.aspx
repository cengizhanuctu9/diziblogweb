﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.Master" AutoEventWireup="true" CodeBehind="bloglar.aspx.cs" Inherits="Diziblog.AdminSayfa.bloglar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>BAŞLIK</th>
            <th>TARİH</th>
            <th>TÜR</th>
            <th>KATEGORİ</th>
            <th>SİL</th>
            <th>GÜNCELLE</th>
        </tr>
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("BLOGID") %></td>
                    <td><%# Eval("BASLIK") %></td>
                    <td><%# Eval("TARIH") %></td>
                    <td><%# Eval("TUR") %></td>
                    <td><%# Eval("KATOGERI") %></td>
                    <td><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#"sil.aspx?BLOGID=" +Eval("BLOGID" )%>' CssClass="btn btn-danger">SİL</asp:HyperLink></td>
                    <td><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%#"guncelle.aspx?BLOGID=" +Eval("BLOGID" )%>' CssClass="btn btn-info">GÜNCELLE</asp:HyperLink></td>

                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <td><a href="blogekle.aspx" class="btn btn-info">Ekle</a></td>
</asp:Content>
