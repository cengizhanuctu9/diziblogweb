﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diziblog.entity;

namespace Diziblog.AdminSayfa
{
    public partial class guncelle : System.Web.UI.Page
    {
        blogdizifilmEntities db = new blogdizifilmEntities();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KULLANICIAD"] == null)
            {
                Response.Redirect("~/giris.aspx");
            }
            int y = int.Parse(Request.QueryString["BLOGID"]);
            if (Page.IsPostBack == false)
            {
                var turler = (from x in db.TBLTUR
                              select new
                              {
                                  x.TURAD,
                                  x.TURID
                              }).ToList();
                DropDownList1.DataSource = turler;
                DropDownList1.DataBind();
                var kategori = (from x in db.TBLKATEGORI
                                select new
                                {
                                    x.KATEGORIAD,
                                    x.KATEGORIID
                                }).ToList();
                DropDownList2.DataSource = kategori;
                DropDownList2.DataBind();

                var deger = db.TBLBLOG.Find(y);
                TextBox1.Text = deger.BASLIK;
                TextBox2.Text = deger.TARIH.ToString();
                TextBox3.Text = deger.GORSEL;
                TextBox4.Text = deger.ICERIK;
                DropDownList1.SelectedValue = deger.TUR.ToString();
                DropDownList2.SelectedValue = deger.KATOGERI.ToString();

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int y = int.Parse(Request.QueryString["BLOGID"]);
            var blog = db.TBLBLOG.Find(y);

            blog.BASLIK = TextBox1.Text;
            blog.TARIH = DateTime.Parse(TextBox2.Text);
            blog.GORSEL = TextBox3.Text;
            blog.ICERIK = TextBox4.Text;
            blog.TUR = byte.Parse(DropDownList1.SelectedValue);
            blog.KATOGERI = byte.Parse(DropDownList2.SelectedValue);
            db.SaveChanges();
            Response.Redirect("bloglar.aspx");
        }
    }
}