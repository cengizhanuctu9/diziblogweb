﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diziblog.entity;

namespace Diziblog.AdminSayfa
{

    public partial class blogekle : System.Web.UI.Page
    {
        blogdizifilmEntities db = new blogdizifilmEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KULLANICIAD"] == null)
            {
                Response.Redirect("~/giris.aspx");
            }
            if (Page.IsPostBack == false)
            {
                var turler = (from x in db.TBLTUR
                              select new
                              {
                                  x.TURAD,
                                  x.TURID
                              }).ToList();
                DropDownList1.DataSource = turler;
                DropDownList1.DataBind();
                var kategori = (from x in db.TBLKATEGORI
                                select new
                                {
                                    x.KATEGORIAD,
                                    x.KATEGORIID
                                }).ToList();
                DropDownList2.DataSource = kategori;
                DropDownList2.DataBind();

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TBLBLOG t = new TBLBLOG();
            t.BASLIK = TextBox1.Text;
            t.TARIH = DateTime.Parse(TextBox2.Text);
            t.GORSEL = TextBox3.Text;
            t.ICERIK = TextBox4.Text;
            t.TUR = byte.Parse(DropDownList1.SelectedValue);
            t.KATOGERI = byte.Parse(DropDownList2.SelectedValue);
            db.TBLBLOG.Add(t);
            db.SaveChanges();
            Response.Redirect("blogekle.aspx");



        }
    }
}