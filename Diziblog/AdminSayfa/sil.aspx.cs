﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diziblog.entity;

namespace Diziblog.AdminSayfa
{
    public partial class sil : System.Web.UI.Page
    {
        blogdizifilmEntities db = new blogdizifilmEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KULLANICIAD"] == null)
            {
                Response.Redirect("~/giris.aspx");
            }
            int x = Convert.ToInt32(Request.QueryString["BLOGID"]);
            var blog = db.TBLBLOG.Find(x);
            db.TBLBLOG.Remove(blog);
            db.SaveChanges();
            Response.Redirect("bloglar.aspx");
        }
    }
}