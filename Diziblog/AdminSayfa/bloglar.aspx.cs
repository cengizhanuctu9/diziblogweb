﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diziblog.entity;
namespace Diziblog.AdminSayfa
{
    public partial class bloglar : System.Web.UI.Page
    {
        blogdizifilmEntities db = new blogdizifilmEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KULLANICIAD"] == null)
            {
                Response.Redirect("~/giris.aspx");
            }
            Repeater1.DataSource = db.TBLBLOG.ToList();
            Repeater1.DataBind();
        }
    }
}