﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diziblog.entity;
namespace Diziblog.AdminSayfa
{
    public partial class yorumguncelle : System.Web.UI.Page
    {
        blogdizifilmEntities db = new blogdizifilmEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KULLANICIAD"] == null)
            {
                Response.Redirect("~/giris.aspx");
            }
            int y = int.Parse(Request.QueryString["YORUMID"]);
            if (Page.IsPostBack == false)
            {
              
                var deger = db.TBLYORUM.Find(y);
                TextBox1.Text = deger.TBLBLOG.BASLIK;
                TextBox2.Text = deger.KULLANICIAD;
                TextBox4.Text = deger.YORUMICERIK;
              

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            int y = int.Parse(Request.QueryString["YORUMID"]);
            var blog = db.TBLYORUM.Find(y);

          
           
            blog.KULLANICIAD = TextBox2.Text;
            blog.YORUMICERIK = TextBox4.Text;
           
            db.SaveChanges();
            Response.Redirect("yorumlar.aspx");
        }
    }
}