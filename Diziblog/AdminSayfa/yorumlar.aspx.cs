﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diziblog.entity;

namespace Diziblog.AdminSayfa
{
    public partial class yorumlar : System.Web.UI.Page
    {
        blogdizifilmEntities db = new blogdizifilmEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KULLANICIAD"] == null)
            {
                Response.Redirect("~/giris.aspx");
            }
            var yorumlar =(from x in db.TBLYORUM select new {x.YORUMID,x.KULLANICIAD,x.TBLBLOG.BASLIK } ).ToList() ;
            Repeater1.DataSource = yorumlar;
            Repeater1.DataBind();
        }
    }
}