﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Anasayfa.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Diziblog.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="content-grids">
            <div class="col-md-8 content-main">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <div class="content-grid">
                            <div class="content-grid-info">
                                <img src="<%# Eval("GORSEL") %>" alt="" style="height: 300px; width: 600px" />
                                <div class="post-info">
                                    <h4><a href="icerik.aspx?BLOGID=<%# Eval("BLOGID") %>"><%# Eval("BASLIK") %></a> "<%# Eval("TARIH") %>"</h4>
                                    <p>"<%# Eval("ICERIK") %>"</p>
                                    <a href="icerik.aspx?BLOGID=<%# Eval("BLOGID") %>"><span></span>DAHA FAZLA OKU </a>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="col-md-4 content-right">
            <div class="recent">
                <h3>SON KONULAR</h3>
                <ul>
                    <asp:Repeater ID="Repeater3" runat="server">
                        <ItemTemplate>
                            <li><a href="#"><%# Eval("BASLIK") %></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="comments">
                <h3>SON YORUMLAR</h3>
                <asp:Repeater ID="Repeater4" runat="server">
                    <ItemTemplate>
                        <ul>
                            <li><a href="#"><%# Eval("KULLANICIAD") %> - <%# Eval("YORUMICERIK") %></a></li>
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
            <div class="clearfix"></div>
           
            <div class="categories">
                <h3>KATEGORİ</h3>
                <ul>
                    <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>
                            <li><a href="kategori.aspx?KATEGORIID=<%# Eval("KATEGORIID") %>"><%# Eval("KATEGORIAD") %></a></li>

                        </ItemTemplate>
                    </asp:Repeater>

                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

</asp:Content>
